**DB:**
- PostgreSQL 12.5

**Create table:**
- CREATE TABLE webhooks_post (id serial PRIMARY KEY, type integer NOT NULL, data jsonb);

**Set Environment Variables:**

- export POSTGRES_URL="host"
- export POSTGRES_USER="nameUser"
- export POSTGRES_PW="password"
- export POSTGRES_DB="nameDB"

**There are three endpoints:**

- http://host/post/webhook/type/first
- http://host/post/webhook/type/second
- http://host/post/webhook/type/third


These endpoints accept post requests with json. Saving data into DB.
