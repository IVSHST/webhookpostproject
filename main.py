from flask import Flask, request
import json
from urllib.parse import unquote
import psycopg2 as pg2
import os

app = Flask(__name__)

@app.route("/post/webhook/type/first", methods=['POST'])
def post_webhook_first():
    if request.method == 'POST':
        type = 1
        requestJson = request.get_data(as_text = True)
        data = unquote(requestJson)[13:].replace("'","\"")
        if unquote(requestJson)[:12] == "webhook_data":
            try:
                conn = pg2.connect(dbname  = get_env_variable("POSTGRES_DB"),
                                   user    = get_env_variable("POSTGRES_USER"),
                                   password= get_env_variable("POSTGRES_PW"),
                                   host    = get_env_variable("POSTGRES_URL"))
            except:
                raise Exception(f"I am unable to connect to the database.")
                exit()

            with conn:
                with conn.cursor() as cur:
                        cur.execute(f"INSERT INTO webhooks_post (type, data) VALUES('{type}','{data}')")
        return ""

@app.route("/post/webhook/type/second", methods=['POST'])
def post_webhook_second():
    if request.method == 'POST':
        type = 2
        requestJson = request.get_data(as_text = True)
        data = unquote(requestJson)[13:].replace("'","\"")
        if unquote(requestJson)[:12] == "webhook_data":
            try:
                conn = pg2.connect(dbname  = get_env_variable("POSTGRES_DB"),
                                   user    = get_env_variable("POSTGRES_USER"),
                                   password= get_env_variable("POSTGRES_PW"),
                                   host    = get_env_variable("POSTGRES_URL"))
            except:
                raise Exception(f"I am unable to connect to the database.")
                exit()

            with conn:
                with conn.cursor() as cur:
                        cur.execute(f"INSERT INTO webhooks_post (type, data) VALUES('{type}','{data}')")
        return ""

@app.route("/post/webhook/type/third", methods=['POST'])
def post_webhook_third():
    if request.method == 'POST':
        type = 3
        requestJson = request.get_data(as_text = True)
        data = unquote(requestJson)[13:].replace("'","\"")
        if unquote(requestJson)[:12] == "webhook_data":
            try:
                conn = pg2.connect(dbname  = get_env_variable("POSTGRES_DB"),
                                   user    = get_env_variable("POSTGRES_USER"),
                                   password= get_env_variable("POSTGRES_PW"),
                                   host    = get_env_variable("POSTGRES_URL"))
            except:
                raise Exception(f"I am unable to connect to the database.")
                exit()

            with conn:
                with conn.cursor() as cur:
                        cur.execute(f"INSERT INTO webhooks_post (type, data) VALUES('{type}','{data}')")
        return ""

def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

if __name__ == '__main__':
    app.run()
